const readline = require('readline')
const fs = require('fs')
const os = require('os')
const http = require('http')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const question = `Choose an option:\n1: Read package.json\n2. Display OS-info\n3. Start HTTP-server\nPlease type a number: `
rl.question(question,chosenOption =>{
    chosenOption = parseInt(chosenOption)
    switch(chosenOption){
        case 1:
            readPackageJSON()
            break
        case 2:
            displayOSInfo()
            break
        case 3:
            startHTTPServer()
            break
        default:
            rl.write("Entered invalid option!")
    }

})

//reads the package.json-file and writes it
function readPackageJSON(){
    rl.write('Reading package.json-file')
    fs.readFile('./package.json', 'utf-8', (error, data) => {
        if (error) rl.write(error)
        else rl.write(data)
        rl.close()
    });

}

//displays the OS-info
function displayOSInfo(){
    rl.write('Getting OS info... \n')

    const totalmem = 'SYSTEM MEMORY: ' +('SYSTEM MEMORY ', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + ' GB')
    const freemem = 'FREE MEMORY ' + ('FREE MEMORY ', (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + ' GB')
    const CPUCores = 'CPU_CORES: ' + ('CPU CORES: ', (os.cpus().length))
    const arch = 'ARCH: ' + os.arch()
    const platform = 'PLATFORM: ' + os.platform()
    const release = 'RELEASE: ' + os.release
    const user = 'USER: ' + os.userInfo().username

    //writes the info
    rl.write(totalmem + '\n' + freemem + '\n' + CPUCores + '\n' + arch + '\n' +platform + '\n' + release + '\n' + user + '\n')
    rl.close()
}

//starts a HTTP-server
function startHTTPServer(){
    rl.write('Starting the HTTP server...\n')
    
    const server = http.createServer((req, res) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        res.end('Hello World');
    })

    rl.write('Listening on port 3000.... \n')
    server.listen(3000)
    rl.close()
}
